/*
--删除表
DROP TABLE sys_dept;
DROP TABLE sys_user;
DROP TABLE sys_post;
DROP TABLE sys_role;
DROP TABLE sys_menu;
DROP TABLE sys_user_role;
DROP TABLE sys_role_menu;
DROP TABLE sys_user_post;
DROP TABLE sys_oper_log;
DROP TABLE sys_dict_type;
DROP TABLE sys_dict_data;
DROP TEBLE sys_config;
DROP TEBLE sys_logininfor;
DROP TEBLE sys_user_online;
DROP TEBLE sys_job;
DROP TEBLE sys_job_log;
DROP TEBLE sys_notice;
*/

-- ----------------------------
-- 1、部门表
-- ----------------------------
CREATE TABLE sys_dept (
  dept_id       NUMBER(11)     NOT NULL,
  parent_id     NUMBER(11)     DEFAULT 0,
  ancestors     NVARCHAR2(50)  DEFAULT '',
  dept_name     NVARCHAR2(30)  DEFAULT '',
  order_num     NUMBER(4)      DEFAULT 0,
  leader        NVARCHAR2(20)  DEFAULT '',
  phone         NVARCHAR2(11)  DEFAULT '',
  email         NVARCHAR2(50)  DEFAULT '',
  status        NVARCHAR2(1)   DEFAULT '0',
  create_by     NVARCHAR2(64)  DEFAULT '',
  create_time   DATE,
  update_by     NVARCHAR2(64)  DEFAULT '',
  update_time   DATE,
  PRIMARY KEY (dept_id)
)
/
comment on table sys_dept is '部门表';
comment on column sys_dept.dept_id is '部门id';
comment on column sys_dept.parent_id is '父部门id';
comment on column sys_dept.ancestors is '祖级列表';
comment on column sys_dept.dept_name is '部门名称';
comment on column sys_dept.order_num is '显示顺序';
comment on column sys_dept.leader is '负责人';
comment on column sys_dept.phone is '联系电话';
comment on column sys_dept.email is '邮箱';
comment on column sys_dept.status is '部门状态（0正常 1停用）';
comment on column sys_dept.create_by is '创建者';
comment on column sys_dept.create_time is '创建时间';
comment on column sys_dept.update_by is '更新者';
comment on column sys_dept.update_time is '更新时间';

-- ----------------------------
-- 初始化-部门表数据
-- ----------------------------
INSERT INTO sys_dept VALUES(100,  0,   '0',         '若依集团', 0, '若依', '15888888888', 'ry@qq.com', '0', 'admin', sysdate, 'ry', sysdate);
INSERT INTO sys_dept VALUES(101,  100, '0,100',     '研发部门', 1, '若依', '15888888888', 'ry@qq.com', '0', 'admin', sysdate, 'ry', sysdate);
INSERT INTO sys_dept VALUES(102,  100, '0,100',     '市场部门', 2, '若依', '15888888888', 'ry@qq.com', '0', 'admin', sysdate, 'ry', sysdate);
INSERT INTO sys_dept VALUES(103,  100, '0,100',     '测试部门', 3, '若依', '15888888888', 'ry@qq.com', '0', 'admin', sysdate, 'ry', sysdate);
INSERT INTO sys_dept VALUES(104,  100, '0,100',     '财务部门', 4, '若依', '15888888888', 'ry@qq.com', '0', 'admin', sysdate, 'ry', sysdate);
INSERT INTO sys_dept VALUES(105,  100, '0,100',     '运维部门', 5, '若依', '15888888888', 'ry@qq.com', '0', 'admin', sysdate, 'ry', sysdate);
INSERT INTO sys_dept VALUES(106,  101, '0,100,101', '研发一部', 1, '若依', '15888888888', 'ry@qq.com', '0', 'admin', sysdate, 'ry', sysdate);
INSERT INTO sys_dept values(107,  101, '0,100,101', '研发二部', 2, '若依', '15888888888', 'ry@qq.com', '0', 'admin', sysdate, 'ry', sysdate);
INSERT INTO sys_dept values(108,  102, '0,100,102', '市场一部', 1, '若依', '15888888888', 'ry@qq.com', '0', 'admin', sysdate, 'ry', sysdate);
INSERT INTO sys_dept values(109,  102, '0,100,102', '市场二部', 2, '若依', '15888888888', 'ry@qq.com', '1', 'admin', sysdate, 'ry', sysdate);

-- ----------------------------
-- 2、用户信息表
-- ----------------------------
CREATE TABLE sys_user (
  user_id       NUMBER(11)     not NULL,
  dept_id       NUMBER(11)     default null,
  login_name    NVARCHAR2(30)  NOT NULL,
  user_name     NVARCHAR2(30)  NOT NULL,
  user_type     NVARCHAR2(2)   DEFAULT '00',
  email         NVARCHAR2(50)  DEFAULT '',
  phonenumber   NVARCHAR2(11)  DEFAULT '',
  sex           NVARCHAR2(1)   DEFAULT '0',
  avatar        NVARCHAR2(100) DEFAULT '',
  password      NVARCHAR2(50)  DEFAULT '',
  salt          NVARCHAR2(20)  DEFAULT '',
  status        NVARCHAR2(1)   DEFAULT '0' ,
  del_flag      NVARCHAR2(1)   DEFAULT '0',
  login_ip      NVARCHAR2(20)  DEFAULT '',
  login_date    DATE           DEFAULT Sysdate,
  create_by     NVARCHAR2(64)  DEFAULT '',
  create_time   DATE           DEFAULT Sysdate,
  update_by     NVARCHAR2(64)  DEFAULT '',
  update_time   DATE           DEFAULT SYSDATE,
  remark        NVARCHAR2(500) DEFAULT '',
  primary key (user_id)
)
/
comment on table sys_user is '用户信息表';
comment on column sys_user.user_id is '用户ID';
comment on column sys_user.dept_id is '部门ID';
comment on column sys_user.login_name is '登录账号';
comment on column sys_user.user_name is '用户昵称';
comment on column sys_user.user_type is '用户类型（00系统用户）';
comment on column sys_user.email is '用户邮箱';
comment on column sys_user.phonenumber is '手机号码';
comment on column sys_user.sex is '用户性别（0男 1女 2未知）';
comment on column sys_user.avatar is '头像路径';
comment on column sys_user.password is '密码';
comment on column sys_user.salt is '盐加密';
comment on column sys_user.status is '帐号状态（0正常 1停用）';
comment on column sys_user.del_flag is '删除标志（0代表存在 2代表删除）';
comment on column sys_user.login_ip is '最后登陆IP';
comment on column sys_user.login_date is '最后登陆时间';
comment on column sys_user.create_by is '创建者';
comment on column sys_user.create_time is '创建时间';
comment on column sys_user.update_by is '更新者';
comment on column sys_user.update_time is '更新时间';
comment on column sys_user.remark is '备注';

-- ----------------------------
-- 初始化-用户信息表数据
-- ----------------------------
INSERT INTO sys_user values(1,  106, 'admin', '若依', '00', 'ry@163.com', '15888888888', '1', '', '29c67a30398638269fe600f73a054934', '111111', '0', '0', '127.0.0.1', sysdate, 'admin', sysdate, 'ry', sysdate, '管理员');
INSERT INTO sys_user values(2,  108, 'ry',    '若依', '00', 'ry@qq.com',  '15666666666', '1', '', '8e6d98b90472783cc73c17047ddccf36', '222222', '0', '0', '127.0.0.1', sysdate, 'admin', sysdate, 'ry', sysdate, '测试员');

-- ----------------------------
-- 3、岗位信息表
-- ----------------------------
create table sys_post
(
  post_id       NUMBER(11)      NOT NULL,
  post_code     NVARCHAR2(64)   NOT NULL,
  post_name     NVARCHAR2(50)   NOT NULL,
  post_sort     NUMBER(4)       NOT NULL,
  status        NVARCHAR2(1)    NOT NULL,
  create_by     NVARCHAR2(64)   DEFAULT '',
  create_time   DATE,
  update_by     NVARCHAR2(64)   DEFAULT '',
  update_time   DATE,
  remark      NVARCHAR2(500)    DEFAULT '',
  primary key (post_id)
)
/
comment on table sys_post is '岗位信息表';
comment on column sys_post.post_id is '岗位编码';
comment on column sys_post.post_code is '岗位编码';
comment on column sys_post.post_name is '岗位名称';
comment on column sys_post.post_sort is '显示顺序';
comment on column sys_post.status is '状态（0正常 1停用）';
comment on column sys_post.create_by is '创建者';
comment on column sys_post.create_time is '创建时间';
comment on column sys_post.update_by is '更新者';
comment on column sys_post.update_time is '更新时间';
comment on column sys_post.remark is '备注';


-- ----------------------------
-- 初始化-岗位信息表数据
-- ----------------------------
INSERT INTO sys_post values(1, 'ceo',  '董事长',    1, '0', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_post values(2, 'se',   '项目经理',  2, '0', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_post values(3, 'hr',   '人力资源',  3, '0', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_post values(4, 'user', '普通员工',  4, '0', 'admin', sysdate, 'ry', sysdate, '');

-- ----------------------------
-- 4、角色信息表
-- ----------------------------
create table sys_role (
  role_id     NUMBER(11)     NOT NULL ,
  role_name   NVARCHAR2(30)  NOT NULL,
  role_key    NVARCHAR2(100) NOT NULL,
  role_sort   NUMBER(4)      NOT NULL,
  status      NVARCHAR2(1)   NOT NULL,
  create_by   NVARCHAR2(64)  DEFAULT '',
  create_time DATE,
  update_by   NVARCHAR2(64)  DEFAULT '',
  update_time DATE,
  remark      NVARCHAR2(500) DEFAULT '',
  primary key (role_id)
)
/
comment on table sys_role is '角色信息表';
comment on column sys_role.role_id is '角色ID';
comment on column sys_role.role_name is '角色名称';
comment on column sys_role.role_key is '角色权限字符串';
comment on column sys_role.role_sort is '显示顺序';
comment on column sys_role.status is '角色状态（0正常 1停用）';
comment on column sys_role.create_by is '创建者';
comment on column sys_role.create_time is '创建时间';
comment on column sys_role.update_by is '更新者';
comment on column sys_role.update_time is '更新时间';
comment on column sys_role.remark is '备注';

-- ----------------------------
-- 初始化-角色信息表数据
-- ----------------------------
INSERT INTO sys_role values('1', '管理员',   'admin',  1,  '0', 'admin', sysdate, 'ry', sysdate, '管理员');
INSERT INTO sys_role values('2', '普通角色', 'common', 2,  '0', 'admin', sysdate, 'ry', sysdate, '普通角色');


-- ----------------------------
-- 5、菜单权限表
-- ----------------------------
create table sys_menu (
  menu_id       NUMBER(11)      NOT NULL,
  menu_name     NVARCHAR2(50)   NOT NULL,
  parent_id     NUMBER(11)      DEFAULT 0,
  order_num     NUMBER(4)       DEFAULT NULL,
  url           NVARCHAR2(200)  DEFAULT '',
  menu_type     NVARCHAR2(1)    DEFAULT '',
  visible       NVARCHAR2(1)    NOT NULL,
  perms         NVARCHAR2(100)  DEFAULT '',
  icon          NVARCHAR2(100)  DEFAULT '',
  create_by     NVARCHAR2(64)   DEFAULT '',
  create_time   DATE,
  update_by     NVARCHAR2(64)   DEFAULT '',
  update_time   DATE,
  remark        NVARCHAR2(500)  DEFAULT '',
  primary key (menu_id)
)
/
comment on table sys_menu is '菜单权限表';
comment on column sys_menu.menu_id is '菜单ID';
comment on column sys_menu.menu_name is '菜单名称';
comment on column sys_menu.parent_id is '父菜单ID';
comment on column sys_menu.order_num is '显示顺序';
comment on column sys_menu.url is '请求地址';
comment on column sys_menu.menu_type is '菜单类型（M目录 C菜单 F按钮）';
comment on column sys_menu.visible is '菜单状态（0显示 1隐藏）';
comment on column sys_menu.perms is '权限标识';
comment on column sys_menu.icon is '菜单图标';
comment on column sys_menu.create_by is '创建者';
comment on column sys_menu.create_time is '创建时间';
comment on column sys_menu.update_by is '更新者';
comment on column sys_menu.update_time is '更新时间';
comment on column sys_menu.remark is '备注';

-- ----------------------------
-- 初始化-菜单信息表数据
-- ----------------------------
-- 一级菜单
INSERT INTO sys_menu values('1', '系统管理', '0', '1', '#', 'M', '0', '', 'fa fa-gear',         'admin', sysdate, 'ry', sysdate, '系统管理目录');
INSERT INTO sys_menu values('2', '系统监控', '0', '2', '#', 'M', '0', '', 'fa fa-video-camera', 'admin', sysdate, 'ry', sysdate, '系统监控目录');
INSERT INTO sys_menu values('3', '系统工具', '0', '3', '#', 'M', '0', '', 'fa fa-bars',         'admin', sysdate, 'ry', sysdate, '系统工具目录');
-- 二级菜单
INSERT INTO sys_menu values('100',  '用户管理', '1', '1', '/system/user',        'C', '0', 'system:user:view',         '#', 'admin', sysdate, 'ry', sysdate, '用户管理菜单');
INSERT INTO sys_menu values('101',  '角色管理', '1', '2', '/system/role',        'C', '0', 'system:role:view',         '#', 'admin', sysdate, 'ry', sysdate, '角色管理菜单');
INSERT INTO sys_menu values('102',  '菜单管理', '1', '3', '/system/menu',        'C', '0', 'system:menu:view',         '#', 'admin', sysdate, 'ry', sysdate, '菜单管理菜单');
INSERT INTO sys_menu values('103',  '部门管理', '1', '4', '/system/dept',        'C', '0', 'system:dept:view',         '#', 'admin', sysdate, 'ry', sysdate, '部门管理菜单');
INSERT INTO sys_menu values('104',  '岗位管理', '1', '5', '/system/post',        'C', '0', 'system:post:view',         '#', 'admin', sysdate, 'ry', sysdate, '岗位管理菜单');
INSERT INTO sys_menu values('105',  '字典管理', '1', '6', '/system/dict',        'C', '0', 'system:dict:view',         '#', 'admin', sysdate, 'ry', sysdate, '字典管理菜单');
INSERT INTO sys_menu values('106',  '参数设置', '1', '7', '/system/config',      'C', '0', 'system:config:view',       '#', 'admin', sysdate, 'ry', sysdate, '参数设置菜单');
INSERT INTO sys_menu values('107',  '通知公告', '1', '8', '/system/notice',      'C', '0', 'system:notice:view',       '#', 'admin', sysdate, 'ry', sysdate, '通知公告菜单');
INSERT INTO sys_menu values('108',  '日志管理', '1', '9', '#',                   'M', '0', '',                         '#', 'admin', sysdate, 'ry', sysdate, '日志管理菜单');
INSERT INTO sys_menu values('109',  '在线用户', '2', '1', '/monitor/online',     'C', '0', 'monitor:online:view',      '#', 'admin', sysdate, 'ry', sysdate, '在线用户菜单');
INSERT INTO sys_menu values('110',  '定时任务', '2', '2', '/monitor/job',        'C', '0', 'monitor:job:view',         '#', 'admin', sysdate, 'ry', sysdate, '定时任务菜单');
INSERT INTO sys_menu values('111',  '数据监控', '2', '3', '/monitor/data',       'C', '0', 'monitor:data:view',        '#', 'admin', sysdate, 'ry', sysdate, '数据监控菜单');
INSERT INTO sys_menu values('112',  '表单构建', '3', '1', '/tool/build',         'C', '0', 'tool:build:view',          '#', 'admin', sysdate, 'ry', sysdate, '表单构建菜单');
INSERT INTO sys_menu values('113',  '代码生成', '3', '2', '/tool/gen',           'C', '0', 'tool:gen:view',            '#', 'admin', sysdate, 'ry', sysdate, '代码生成菜单');
INSERT INTO sys_menu values('114',  '系统接口', '3', '3', '/tool/swagger',       'C', '0', 'tool:swagger:view',        '#', 'admin', sysdate, 'ry', sysdate, '系统接口菜单');
-- 三级菜单
INSERT INTO sys_menu values('500',  '操作日志', '108', '1', '/monitor/operlog',    'C', '0', 'monitor:operlog:view',     '#', 'admin', sysdate, 'ry', sysdate, '操作日志菜单');
INSERT INTO sys_menu values('501',  '登录日志', '108', '2', '/monitor/logininfor', 'C', '0', 'monitor:logininfor:view',  '#', 'admin', sysdate, 'ry', sysdate, '登录日志菜单');
-- 用户管理按钮
INSERT INTO sys_menu values('1000', '用户查询', '100', '1',  '#',  'F', '0', 'system:user:list',        '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1001', '用户新增', '100', '2',  '#',  'F', '0', 'system:user:add',         '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1002', '用户修改', '100', '3',  '#',  'F', '0', 'system:user:edit',        '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1003', '用户删除', '100', '4',  '#',  'F', '0', 'system:user:remove',      '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1004', '用户导出', '100', '5',  '#',  'F', '0', 'system:user:export',      '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1005', '重置密码', '100', '5',  '#',  'F', '0', 'system:user:resetPwd',    '#', 'admin', sysdate, 'ry', sysdate, '');
-- 角色管理按钮
INSERT INTO sys_menu values('1006', '角色查询', '101', '1',  '#',  'F', '0', 'system:role:list',        '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1007', '角色新增', '101', '2',  '#',  'F', '0', 'system:role:add',         '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1008', '角色修改', '101', '3',  '#',  'F', '0', 'system:role:edit',        '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1009', '角色删除', '101', '4',  '#',  'F', '0', 'system:role:remove',      '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1010', '角色导出', '101', '4',  '#',  'F', '0', 'system:role:export',      '#', 'admin', sysdate, 'ry', sysdate, '');
-- 菜单管理按钮
INSERT INTO sys_menu values('1011', '菜单查询', '102', '1',  '#',  'F', '0', 'system:menu:list',        '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1012', '菜单新增', '102', '2',  '#',  'F', '0', 'system:menu:add',         '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1013', '菜单修改', '102', '3',  '#',  'F', '0', 'system:menu:edit',        '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1014', '菜单删除', '102', '4',  '#',  'F', '0', 'system:menu:remove',      '#', 'admin', sysdate, 'ry', sysdate, '');
-- 部门管理按钮
INSERT INTO sys_menu values('1015', '部门查询', '103', '1',  '#',  'F', '0', 'system:dept:list',        '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1016', '部门新增', '103', '2',  '#',  'F', '0', 'system:dept:add',         '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1017', '部门修改', '103', '3',  '#',  'F', '0', 'system:dept:edit',        '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1018', '部门删除', '103', '4',  '#',  'F', '0', 'system:dept:remove',      '#', 'admin', sysdate, 'ry', sysdate, '');
-- 岗位管理按钮
INSERT INTO sys_menu values('1019', '岗位查询', '104', '1',  '#',  'F', '0', 'system:post:list',        '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1020', '岗位新增', '104', '2',  '#',  'F', '0', 'system:post:add',         '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1021', '岗位修改', '104', '3',  '#',  'F', '0', 'system:post:edit',        '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1022', '岗位删除', '104', '4',  '#',  'F', '0', 'system:post:remove',      '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1023', '岗位导出', '104', '4',  '#',  'F', '0', 'system:post:export',      '#', 'admin', sysdate, 'ry', sysdate, '');
-- 字典管理按钮
INSERT INTO sys_menu values('1024', '字典查询', '105', '1', '#',  'F', '0', 'system:dict:list',         '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1025', '字典新增', '105', '2', '#',  'F', '0', 'system:dict:add',          '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1026', '字典修改', '105', '3', '#',  'F', '0', 'system:dict:edit',         '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1027', '字典删除', '105', '4', '#',  'F', '0', 'system:dict:remove',       '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1028', '字典导出', '105', '4', '#',  'F', '0', 'system:dict:export',       '#', 'admin', sysdate, 'ry', sysdate, '');
-- 参数设置按钮
INSERT INTO sys_menu values('1029', '参数查询', '106', '1', '#',  'F', '0', 'system:config:list',      '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1030', '参数新增', '106', '2', '#',  'F', '0', 'system:config:add',       '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1031', '参数修改', '106', '3', '#',  'F', '0', 'system:config:edit',      '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1032', '参数删除', '106', '4', '#',  'F', '0', 'system:config:remove',    '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1033', '参数导出', '106', '4', '#',  'F', '0', 'system:config:export',    '#', 'admin', sysdate, 'ry', sysdate, '');
-- 通知公告按钮
INSERT INTO sys_menu values('1034', '公告查询', '107', '1', '#',  'F', '0', 'system:notice:list',      '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1035', '公告新增', '107', '2', '#',  'F', '0', 'system:notice:add',       '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1036', '公告修改', '107', '3', '#',  'F', '0', 'system:notice:edit',      '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1037', '公告删除', '107', '4', '#',  'F', '0', 'system:notice:remove',    '#', 'admin', sysdate, 'ry', sysdate, '');
-- 操作日志按钮
INSERT INTO sys_menu values('1038', '操作查询', '500', '1', '#',  'F', '0', 'monitor:operlog:list',    '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1039', '操作删除', '500', '2', '#',  'F', '0', 'monitor:operlog:remove',  '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1040', '详细信息', '500', '3', '#',  'F', '0', 'monitor:operlog:detail',  '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1041', '日志导出', '500', '3', '#',  'F', '0', 'monitor:operlog:export',  '#', 'admin', sysdate, 'ry', sysdate, '');
-- 登录日志按钮
INSERT INTO sys_menu values('1042', '登录查询', '501', '1', '#',  'F', '0', 'monitor:logininfor:list',         '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1043', '登录删除', '501', '2', '#',  'F', '0', 'monitor:logininfor:remove',       '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1044', '日志导出', '501', '2', '#',  'F', '0', 'monitor:logininfor:export',       '#', 'admin', sysdate, 'ry', sysdate, '');
-- 在线用户按钮
INSERT INTO sys_menu values('1045', '在线查询', '109', '1', '#',  'F', '0', 'monitor:online:list',             '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1046', '批量强退', '109', '2', '#',  'F', '0', 'monitor:online:batchForceLogout', '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1047', '单条强退', '109', '3', '#',  'F', '0', 'monitor:online:forceLogout',      '#', 'admin', sysdate, 'ry', sysdate, '');
-- 定时任务按钮
INSERT INTO sys_menu values('1048', '任务查询', '110', '1', '#',  'F', '0', 'monitor:job:list',                '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1049', '任务新增', '110', '2', '#',  'F', '0', 'monitor:job:add',                 '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1050', '任务修改', '110', '3', '#',  'F', '0', 'monitor:job:edit',                '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1051', '任务删除', '110', '4', '#',  'F', '0', 'monitor:job:remove',              '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1052', '状态修改', '110', '5', '#',  'F', '0', 'monitor:job:changeStatus',        '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1053', '任务导出', '110', '5', '#',  'F', '0', 'monitor:job:export',              '#', 'admin', sysdate, 'ry', sysdate, '');
-- 代码生成按钮
INSERT INTO sys_menu values('1054', '生成查询', '113', '1', '#',  'F', '0', 'tool:gen:list',  '#', 'admin', sysdate, 'ry', sysdate, '');
INSERT INTO sys_menu values('1055', '生成代码', '113', '2', '#',  'F', '0', 'tool:gen:code',  '#', 'admin', sysdate, 'ry', sysdate, '');

-- ----------------------------
-- 6、用户和角色关联表  用户N-1角色
-- ----------------------------
create table sys_user_role (
  user_id 	NUMBER(11) NOT NULL,
  role_id 	NUMBER(11) NOT NULL,
  primary key(user_id, role_id)
)
/
comment on table sys_user_role is '用户和角色关联表';
comment on column sys_user_role.user_id is '用户ID';
comment on column sys_user_role.role_id is '角色ID';

-- ----------------------------
-- 初始化-用户和角色关联表数据
-- ----------------------------
INSERT INTO sys_user_role values ('1', '1');
INSERT INTO sys_user_role values ('2', '2');

-- ----------------------------
-- 7、角色和菜单关联表  角色1-N菜单
-- ----------------------------
create table sys_role_menu (
  role_id 	NUMBER(11) NOT NULL,
  menu_id 	NUMBER(11) NOT NULL,
  primary key(role_id, menu_id)
)
/
comment on table sys_role_menu is '角色和菜单关联表';
comment on column sys_role_menu.role_id is '角色ID';
comment on column sys_role_menu.menu_id is '菜单ID';

-- ----------------------------
-- 初始化-角色和菜单关联表数据
-- ----------------------------
INSERT INTO sys_role_menu values ('1', '1');
INSERT INTO sys_role_menu values ('1', '2');
INSERT INTO sys_role_menu values ('1', '3');
INSERT INTO sys_role_menu values ('1', '100');
INSERT INTO sys_role_menu values ('1', '101');
INSERT INTO sys_role_menu values ('1', '102');
INSERT INTO sys_role_menu values ('1', '103');
INSERT INTO sys_role_menu values ('1', '104');
INSERT INTO sys_role_menu values ('1', '105');
INSERT INTO sys_role_menu values ('1', '106');
INSERT INTO sys_role_menu values ('1', '107');
INSERT INTO sys_role_menu values ('1', '108');
INSERT INTO sys_role_menu values ('1', '109');
INSERT INTO sys_role_menu values ('1', '110');
INSERT INTO sys_role_menu values ('1', '111');
INSERT INTO sys_role_menu values ('1', '112');
INSERT INTO sys_role_menu values ('1', '113');
INSERT INTO sys_role_menu values ('1', '114');
INSERT INTO sys_role_menu values ('1', '500');
INSERT INTO sys_role_menu values ('1', '501');
INSERT INTO sys_role_menu values ('1', '1000');
INSERT INTO sys_role_menu values ('1', '1001');
INSERT INTO sys_role_menu values ('1', '1002');
INSERT INTO sys_role_menu values ('1', '1003');
INSERT INTO sys_role_menu values ('1', '1004');
INSERT INTO sys_role_menu values ('1', '1005');
INSERT INTO sys_role_menu values ('1', '1006');
INSERT INTO sys_role_menu values ('1', '1007');
INSERT INTO sys_role_menu values ('1', '1008');
INSERT INTO sys_role_menu values ('1', '1009');
INSERT INTO sys_role_menu values ('1', '1010');
INSERT INTO sys_role_menu values ('1', '1011');
insert into sys_role_menu values ('1', '1012');
insert into sys_role_menu values ('1', '1013');
insert into sys_role_menu values ('1', '1014');
insert into sys_role_menu values ('1', '1015');
insert into sys_role_menu values ('1', '1016');
insert into sys_role_menu values ('1', '1017');
insert into sys_role_menu values ('1', '1018');
insert into sys_role_menu values ('1', '1019');
insert into sys_role_menu values ('1', '1020');
insert into sys_role_menu values ('1', '1021');
insert into sys_role_menu values ('1', '1022');
insert into sys_role_menu values ('1', '1023');
insert into sys_role_menu values ('1', '1024');
insert into sys_role_menu values ('1', '1025');
insert into sys_role_menu values ('1', '1026');
insert into sys_role_menu values ('1', '1027');
insert into sys_role_menu values ('1', '1028');
insert into sys_role_menu values ('1', '1029');
insert into sys_role_menu values ('1', '1030');
insert into sys_role_menu values ('1', '1031');
insert into sys_role_menu values ('1', '1032');
insert into sys_role_menu values ('1', '1033');
insert into sys_role_menu values ('1', '1034');
insert into sys_role_menu values ('1', '1035');
insert into sys_role_menu values ('1', '1036');
insert into sys_role_menu values ('1', '1037');
insert into sys_role_menu values ('1', '1038');
insert into sys_role_menu values ('1', '1039');
insert into sys_role_menu values ('1', '1040');
insert into sys_role_menu values ('1', '1041');
insert into sys_role_menu values ('1', '1042');
insert into sys_role_menu values ('1', '1043');
insert into sys_role_menu values ('1', '1044');
insert into sys_role_menu values ('1', '1045');
insert into sys_role_menu values ('1', '1046');
insert into sys_role_menu values ('1', '1047');
insert into sys_role_menu values ('1', '1048');
insert into sys_role_menu values ('1', '1049');
insert into sys_role_menu values ('1', '1050');
insert into sys_role_menu values ('1', '1051');
insert into sys_role_menu values ('1', '1052');
insert into sys_role_menu values ('1', '1053');
insert into sys_role_menu values ('1', '1054');
insert into sys_role_menu values ('1', '1055');

-- ----------------------------
-- 8、用户与岗位关联表  用户1-N岗位
-- ----------------------------
create table sys_user_post
(
	user_id NVARCHAR2(64) NOT NULL,
	post_id NVARCHAR2(64) NOT NULL,
	primary key (user_id, post_id)
)
/
comment on table sys_user_post is '用户与岗位关联表';
comment on column sys_user_post.user_id is '用户ID';
comment on column sys_user_post.post_id is '岗位ID';

-- ----------------------------
-- 初始化-用户与岗位关联表数据
-- ----------------------------
insert into sys_user_post values ('1', '1');
insert into sys_user_post values ('2', '2');

-- ----------------------------
-- 9、操作日志记录
-- ----------------------------
CREATE TABLE sys_oper_log (
  oper_id        NUMBER(11)      NOT NULL,
  title          NVARCHAR2(50)   DEFAULT '',
  business_type  NUMBER(2)       DEFAULT 0,
  method         NVARCHAR2(100)  DEFAULT '',
  operator_type  NUMBER(1)       DEFAULT 0,
  oper_name      NVARCHAR2(50)   DEFAULT '',
  dept_name      NVARCHAR2(50)   DEFAULT '',
  oper_url       NVARCHAR2(255)  DEFAULT '',
  oper_ip        NVARCHAR2(30)   DEFAULT '',
  oper_location  NVARCHAR2(255)  DEFAULT '',
  oper_param     NVARCHAR2(255)  DEFAULT '',
  status         NUMBER(1)       DEFAULT 0,
  error_msg      NVARCHAR2(2000) DEFAULT '',
  oper_time      DATE,
  PRIMARY KEY (oper_id)
)
/
comment on table sys_oper_log is '操作日志记录';
comment on column sys_oper_log.oper_id is '日志主键';
comment on column sys_oper_log.title is '模块标题';
comment on column sys_oper_log.business_type is '业务类型（0其它 1新增 2修改 3删除）';
comment on column sys_oper_log.method is '方法名称';
comment on column sys_oper_log.operator_type is '操作类别（0其它 1后台用户 2手机端用户）';
comment on column sys_oper_log.oper_name is '操作人员';
comment on column sys_oper_log.dept_name is '部门名称';
comment on column sys_oper_log.oper_url is '请求URL';
comment on column sys_oper_log.oper_ip is '主机地址';
comment on column sys_oper_log.oper_location is '操作地点';
comment on column sys_oper_log.oper_param is '请求参数';
comment on column sys_oper_log.status is '操作状态（0正常 1异常）';
comment on column sys_oper_log.error_msg is '错误消息';
comment on column sys_oper_log.oper_time is '操作时间';
-- ----------------------------
-- 10、字典类型表
-- ----------------------------
create table sys_dict_type
(
  dict_id      NUMBER(11)     NOT NULL,
  dict_name    NVARCHAR2(100) DEFAULT '',
  dict_type    NVARCHAR2(100) DEFAULT '',
  status       NVARCHAR2(1)   DEFAULT '0',
  create_by    NVARCHAR2(64)  DEFAULT '',
  create_time  DATE,
  update_by    NVARCHAR2(64)  DEFAULT '',
  update_time  DATE,
  remark       NVARCHAR2(500) DEFAULT '',
  primary key (dict_id),
  unique (dict_type)
)
/
comment on table sys_dict_type is '字典类型表';
comment on column sys_dict_type.dict_id is '日志主键';
comment on column sys_dict_type.dict_name is '日志主键';
comment on column sys_dict_type.dict_type is '日志主键';
comment on column sys_dict_type.status is '日志主键';
comment on column sys_dict_type.create_by is '日志主键';
comment on column sys_dict_type.create_time is '日志主键';
comment on column sys_dict_type.update_by is '日志主键';
comment on column sys_dict_type.update_time is '日志主键';
comment on column sys_dict_type.remark is '日志主键';

insert into sys_dict_type values(1,  '用户性别', 'sys_user_sex',        '0', 'admin', sysdate, 'ry', sysdate, '用户性别列表');
insert into sys_dict_type values(2,  '菜单状态', 'sys_show_hide',       '0', 'admin', sysdate, 'ry', sysdate, '菜单状态列表');
insert into sys_dict_type values(3,  '系统开关', 'sys_normal_disable',  '0', 'admin', sysdate, 'ry', sysdate, '系统开关列表');
insert into sys_dict_type values(4,  '任务状态', 'sys_job_status',      '0', 'admin', sysdate, 'ry', sysdate, '任务状态列表');
insert into sys_dict_type values(5,  '系统是否', 'sys_yes_no',          '0', 'admin', sysdate, 'ry', sysdate, '系统是否列表');
insert into sys_dict_type values(6,  '通知类型', 'sys_notice_type',     '0', 'admin', sysdate, 'ry', sysdate, '通知类型列表');
insert into sys_dict_type values(7,  '通知状态', 'sys_notice_status',   '0', 'admin', sysdate, 'ry', sysdate, '通知状态列表');
insert into sys_dict_type values(8,  '操作类型', 'sys_oper_type',       '0', 'admin', sysdate, 'ry', sysdate, '操作类型列表');
insert into sys_dict_type values(9,  '系统状态', 'sys_common_status',   '0', 'admin', sysdate, 'ry', sysdate, '登录状态列表');

-- ----------------------------
-- 11、字典数据表
-- ----------------------------
create table sys_dict_data
(
  dict_code        NUMBER(11)     NOT NULL,
  dict_sort        NUMBER(4)      DEFAULT 0,
  dict_label       NVARCHAR2(100) DEFAULT '',
  dict_value       NVARCHAR2(100) DEFAULT '',
  dict_type        NVARCHAR2(100) DEFAULT '',
  css_class        NVARCHAR2(500) DEFAULT '',
  list_class       NVARCHAR2(500) DEFAULT '',
  is_DEFAULT       NVARCHAR2(1)   DEFAULT 'N',
  status           NVARCHAR2(1)   DEFAULT '0',
  create_by        NVARCHAR2(64)  DEFAULT '',
  create_time      DATE,
  update_by        NVARCHAR2(64)  DEFAULT '',
  update_time      DATE,
  remark           NVARCHAR2(500) DEFAULT '',
  primary key (dict_code)
)
/
comment on table sys_dict_data is '字典数据表';
comment on column sys_dict_data.dict_code is '字典编码';
comment on column sys_dict_data.dict_sort is '字典排序';
comment on column sys_dict_data.dict_label is '字典标签';
comment on column sys_dict_data.dict_value is '字典键值';
comment on column sys_dict_data.dict_type is '字典类型';
comment on column sys_dict_data.css_class is '回显样式';
comment on column sys_dict_data.list_class is '日志主键';
comment on column sys_dict_data.is_DEFAULT is '是否默认（Y是 N否）';
comment on column sys_dict_data.status is '状态（0正常 1停用）';
comment on column sys_dict_data.create_by is '创建者';
comment on column sys_dict_data.create_time is '创建时间';
comment on column sys_dict_data.update_by is '修改者';
comment on column sys_dict_data.update_time is '修改时间';
comment on column sys_dict_data.remark is '备注';

insert into sys_dict_data values(1,  1,  '男',       '0',  'sys_user_sex',        '',                                 '',        'Y', '0', 'admin', sysdate, 'ry', sysdate, '性别男');
insert into sys_dict_data values(2,  2,  '女',       '1',  'sys_user_sex',        '',                                 '',        'N', '0', 'admin', sysdate, 'ry', sysdate, '性别女');
insert into sys_dict_data values(3,  3,  '未知',     '2',  'sys_user_sex',        '',                                 '',        'N', '0', 'admin', sysdate, 'ry', sysdate, '性别未知');
insert into sys_dict_data values(4,  1,  '显示',     '0',  'sys_show_hide',       'radio radio-info radio-inline',    'primary', 'Y', '0', 'admin', sysdate, 'ry', sysdate, '显示菜单');
insert into sys_dict_data values(5,  2,  '隐藏',     '1',  'sys_show_hide',       'radio radio-danger radio-inline',  'danger',  'N', '0', 'admin', sysdate, 'ry', sysdate, '隐藏菜单');
insert into sys_dict_data values(6,  1,  '正常',     '0',  'sys_normal_disable',  'radio radio-info radio-inline',    'primary', 'Y', '0', 'admin', sysdate, 'ry', sysdate, '正常状态');
insert into sys_dict_data values(7,  2,  '停用',     '1',  'sys_normal_disable',  'radio radio-danger radio-inline',  'danger',  'N', '0', 'admin', sysdate, 'ry', sysdate, '停用状态');
insert into sys_dict_data values(8,  1,  '正常',     '0',  'sys_job_status',      'radio radio-info radio-inline',    'primary', 'Y', '0', 'admin', sysdate, 'ry', sysdate, '正常状态');
insert into sys_dict_data values(9,  2,  '暂停',     '1',  'sys_job_status',      'radio radio-danger radio-inline',  'danger',  'N', '0', 'admin', sysdate, 'ry', sysdate, '停用状态');
insert into sys_dict_data values(10, 1,  '是',       'Y',  'sys_yes_no',          'radio radio-info radio-inline',    'primary', 'Y', '0', 'admin', sysdate, 'ry', sysdate, '系统默认是');
insert into sys_dict_data values(11, 2,  '否',       'N',  'sys_yes_no',          'radio radio-danger radio-inline',  'danger',  'N', '0', 'admin', sysdate, 'ry', sysdate, '系统默认否');
insert into sys_dict_data values(12, 1,  '通知',     '1',  'sys_notice_type',     '',                                 'warning', 'Y', '0', 'admin', sysdate, 'ry', sysdate, '通知');
insert into sys_dict_data values(13, 2,  '公告',     '2',  'sys_notice_type',     '',                                 'success', 'N', '0', 'admin', sysdate, 'ry', sysdate, '公告');
insert into sys_dict_data values(14, 1,  '正常',     '0',  'sys_notice_status',   'radio radio-info radio-inline',    'primary', 'Y', '0', 'admin', sysdate, 'ry', sysdate, '正常状态');
insert into sys_dict_data values(15, 2,  '关闭',     '1',  'sys_notice_status',   'radio radio-danger radio-inline',  'danger',  'N', '0', 'admin', sysdate, 'ry', sysdate, '关闭状态');
insert into sys_dict_data values(16, 1,  '新增',     '1',  'sys_oper_type',        '',                                'info',    'N', '0', 'admin', sysdate, 'ry', sysdate, '新增操作');
insert into sys_dict_data values(17, 2,  '修改',     '2',  'sys_oper_type',        '',                                'info',    'N', '0', 'admin', sysdate, 'ry', sysdate, '新增操作');
insert into sys_dict_data values(18, 3,  '删除',     '3',  'sys_oper_type',        '',                                'danger',  'N', '0', 'admin', sysdate, 'ry', sysdate, '新增操作');
insert into sys_dict_data values(19, 4,  '授权',     '4',  'sys_oper_type',        '',                                'primary', 'N', '0', 'admin', sysdate, 'ry', sysdate, '新增操作');
insert into sys_dict_data values(20, 5,  '导出',     '5',  'sys_oper_type',        '',                                'warning', 'N', '0', 'admin', sysdate, 'ry', sysdate, '新增操作');
insert into sys_dict_data values(21, 6,  '导入',     '6',  'sys_oper_type',        '',                                'warning', 'N', '0', 'admin', sysdate, 'ry', sysdate, '新增操作');
insert into sys_dict_data values(22, 7,  '强退',     '7',  'sys_oper_type',        '',                                'danger',  'N', '0', 'admin', sysdate, 'ry', sysdate, '新增操作');
insert into sys_dict_data values(23, 8,  '生成代码', '8',  'sys_oper_type',        '',                                'warning', 'N', '0', 'admin', sysdate, 'ry', sysdate, '新增操作');
insert into sys_dict_data values(24, 1,  '成功',     '0',  'sys_common_status',    '',                                'primary', 'N', '0', 'admin', sysdate, 'ry', sysdate, '正常状态');
insert into sys_dict_data values(25, 2,  '失败',     '1',  'sys_common_status',    '',                                'danger',  'N', '0', 'admin', sysdate, 'ry', sysdate, '停用状态');

-- ----------------------------
-- 12、参数配置表
-- ----------------------------
create table sys_config (
  config_id     NUMBER(5)       NOT NULL ,
  config_name   NVARCHAR2(100)  DEFAULT '',
  config_key    NVARCHAR2(100)  DEFAULT '',
  config_value  NVARCHAR2(100)  DEFAULT '',
  config_type   NVARCHAR2(1)    DEFAULT 'N' ,
  create_by     NVARCHAR2(64)   DEFAULT '',
  create_time   DATE,
  update_by     NVARCHAR2(64)   DEFAULT '',
  update_time   DATE,
  remark        NVARCHAR2(500)  DEFAULT '',
  primary key (config_id)
)
/
comment on table sys_config is '参数配置表';
comment on column sys_config.config_id is '参数主键';
comment on column sys_config.config_name is '参数名称';
comment on column sys_config.config_key is '参数键名';
comment on column sys_config.config_value is '参数键值';
comment on column sys_config.config_type is '系统内置（Y是 N否）';
comment on column sys_config.create_by is '创建者';
comment on column sys_config.create_time is '创建时间';
comment on column sys_config.update_by is '更新者';
comment on column sys_config.update_time is '更新时间';
comment on column sys_config.remark is '备注';

insert into sys_config values(1, '主框架页-默认皮肤样式名称', 'sys.index.skinName',     'skin-default',  'Y', 'admin', sysdate, 'ry', sysdate, '默认 skin-default、蓝色 skin-blue、黄色 skin-yellow' );
insert into sys_config values(2, '用户管理-账号初始密码',     'sys.user.initPassword',  '123456',        'Y', 'admin', sysdate, 'ry', sysdate, '初始化密码 123456' );

-- ----------------------------
-- 13、系统访问记录
-- ----------------------------
create table sys_logininfor (
  info_id      NUMBER(11)      NOT NULL,
  login_name   NVARCHAR2(50)   DEFAULT '',
  ipaddr       NVARCHAR2(50)   DEFAULT '',
  login_location NVARCHAR2(255)DEFAULT '',
  browser      NVARCHAR2(50)   DEFAULT '',
  os           NVARCHAR2(50)   DEFAULT '',
  status       NVARCHAR2(1)    DEFAULT '0',
  msg          NVARCHAR2(255)  DEFAULT '',
  login_time   DATE,
  primary key (info_id)
)
/
comment on table sys_logininfor is '系统访问记录';
comment on column sys_logininfor.info_id is '访问ID';
comment on column sys_logininfor.login_name is '登录账号';
comment on column sys_logininfor.ipaddr is '登录IP地址';
comment on column sys_logininfor.login_location is '登录地点';
comment on column sys_logininfor.browser is '浏览器类型';
comment on column sys_logininfor.os is '操作系统';
comment on column sys_logininfor.status is '登录状态（0成功 1失败）';
comment on column sys_logininfor.msg is '提示消息';
comment on column sys_logininfor.login_time is '访问时间';

-- ----------------------------
-- 14、在线用户记录
-- ----------------------------
create table sys_user_online (
  sessionId         NVARCHAR2(50)  DEFAULT '',
  login_name        NVARCHAR2(50)  DEFAULT '',
  dept_name         NVARCHAR2(50)  DEFAULT '',
  ipaddr            NVARCHAR2(50)  DEFAULT '',
  login_location    NVARCHAR2(255) DEFAULT '',
  browser           NVARCHAR2(50)  DEFAULT '',
  os                NVARCHAR2(50)  DEFAULT '',
  status            NVARCHAR2(10)  DEFAULT '',
  start_timestsamp  DATE ,
  last_access_time  DATE ,
  expire_time       NUMBER(5)      DEFAULT 0,
  primary key (sessionId)
)
/
comment on table sys_user_online is '在线用户记录';
comment on column sys_user_online.sessionId is '用户会话id';
comment on column sys_user_online.login_name is '登录账号';
comment on column sys_user_online.dept_name is '部门名称';
comment on column sys_user_online.ipaddr is '登录IP地址';
comment on column sys_user_online.login_location is '登录地点';
comment on column sys_user_online.browser is '浏览器类型';
comment on column sys_user_online.os is '操作系统';
comment on column sys_user_online.status is '在线状态on_line在线off_line离线';
comment on column sys_user_online.start_timestsamp is 'session创建时间';
comment on column sys_user_online.last_access_time is 'session最后访问时间';
comment on column sys_user_online.expire_time is '超时时间，单位为分钟';


-- ----------------------------
-- 15、定时任务调度表
-- ----------------------------
create table sys_job (
  job_id          NUMBER(11)       NOT NULL ,
  job_name            NVARCHAR2(64)   DEFAULT '',
  job_group           NVARCHAR2(64)   DEFAULT '',
  method_name         NVARCHAR2(500)  DEFAULT '',
  method_params       NVARCHAR2(200)  DEFAULT '',
  cron_expression     NVARCHAR2(255)  DEFAULT '',
  misfire_policy      NVARCHAR2(20)   DEFAULT '0',
  status              NVARCHAR2(1)       DEFAULT '0',
  create_by           NVARCHAR2(64)   DEFAULT '',
  create_time         DATE,
  update_by           NVARCHAR2(64)   DEFAULT '',
  update_time         date                      ,
  remark              NVARCHAR2(500)  DEFAULT '',
  primary key (job_id, job_name, job_group)
)
/
comment on table sys_job is '定时任务调度表';
comment on column sys_job.job_id is '任务ID';
comment on column sys_job.job_name is '任务名称';
comment on column sys_job.job_group is '任务组名';
comment on column sys_job.method_name is '任务方法';
comment on column sys_job.method_params is '方法参数';
comment on column sys_job.cron_expression is 'cron执行表达式';
comment on column sys_job.misfire_policy is '计划执行错误策略（0默认 1继续 2等待 3放弃）';
comment on column sys_job.status is '状态（0正常 1暂停）';
comment on column sys_job.create_by is '创建者';
comment on column sys_job.create_time is '创建时间';
comment on column sys_job.update_by is '更新者';
comment on column sys_job.update_time is '更新时间';
comment on column sys_job.remark is '备注信息';

insert into sys_job values(1, 'ryTask', '系统默认（无参）', 'ryNoParams',  '',   '0/10 * * * * ?', '0', '1', 'admin', sysdate, 'ry', sysdate, '');
insert into sys_job values(2, 'ryTask', '系统默认（有参）', 'ryParams',    'ry', '0/20 * * * * ?', '0', '1', 'admin', sysdate, 'ry', sysdate, '');


-- ----------------------------
-- 16、定时任务调度日志表
-- ----------------------------
create table sys_job_log (
  job_log_id          NUMBER(11)      NOT NULL,
  job_name            NVARCHAR2(64)   NOT NULL,
  job_group           NVARCHAR2(64)   NOT NULL,
  method_name         NVARCHAR2(500),
  method_params       NVARCHAR2(200)  DEFAULT '',
  job_message         NVARCHAR2(500),
  status              NVARCHAR2(1)    DEFAULT '0',
  exception_info      NVARCHAR2(500),
  create_time         DATE,
  primary key (job_log_id)
)
/
comment on table sys_job_log is '定时任务调度日志表';
comment on column sys_job_log.job_log_id is '任务日志ID';
comment on column sys_job_log.job_name is '任务名称';
comment on column sys_job_log.job_group is '任务组名';
comment on column sys_job_log.method_name is '任务方法';
comment on column sys_job_log.method_params is '方法参数';
comment on column sys_job_log.job_message is '日志信息';
comment on column sys_job_log.status is '执行状态（0正常 1失败）';
comment on column sys_job_log.exception_info is '异常信息';
comment on column sys_job_log.create_time is '创建时间';

-- ----------------------------
-- 17、通知公告表
-- ----------------------------
create table sys_notice (
  notice_id       NUMBER(4)       NOT NULL ,
  notice_title    NVARCHAR2(50)   NOT NULL,
  notice_type     NVARCHAR2(2)    NOT NULL,
  notice_content  NVARCHAR2(500)  NOT NULL,
  status          NVARCHAR2(1)    DEFAULT '0',
  create_by       NVARCHAR2(64)   DEFAULT '',
  create_time     DATE,
  update_by       NVARCHAR2(64)   DEFAULT '',
  update_time     DATE,
  remark          NVARCHAR2(255)  DEFAULT '',
  primary key (notice_id)
)
/
comment on table sys_notice is '通知公告表';
comment on column sys_notice.notice_id is '公告ID';
comment on column sys_notice.notice_title is '公告标题';
comment on column sys_notice.notice_type is '公告类型（1通知 2公告）';
comment on column sys_notice.notice_content is '公告内容';
comment on column sys_notice.status is '公告状态（0正常 1关闭）';
comment on column sys_notice.create_by is '创建者';
comment on column sys_notice.create_time is '创建时间';
comment on column sys_notice.update_by is '更新者';
comment on column sys_notice.update_time is '更新时间';
comment on column sys_notice.remark is '备注';
-- ----------------------------
-- 初始化-公告信息表数据
-- ----------------------------
insert into sys_notice values('1', '温馨提醒：2018-07-01 若依新版本发布啦', '2', '新版本内容', '0', 'admin', sysdate, 'ry', sysdate, '管理员');
insert into sys_notice values('2', '维护通知：2018-07-01 若依系统凌晨维护', '1', '维护内容',   '0', 'admin', sysdate, 'ry', sysdate, '管理员');

/

--主键队列
CREATE SEQUENCE SEQ_TEST
INCREMENT BY 1 -- 每次加几个
START WITH 10 -- 从1开始计数
NOMAXvalue -- 不设置最大值
NOCYCLE -- 一直累加，不循环
CACHE 10;
/
--主键队列
CREATE SEQUENCE seq_DeptID
INCREMENT BY 1 -- 每次加几个
START WITH 10 -- 从1开始计数
NOMAXvalue -- 不设置最大值
NOCYCLE -- 一直累加，不循环
CACHE 10;
/

--主键队列
CREATE SEQUENCE seq_OperID
INCREMENT BY 1 -- 每次加几个
START WITH 10 -- 从1开始计数
NOMAXvalue -- 不设置最大值
NOCYCLE -- 一直累加，不循环
CACHE 10;
/

--主键队列
CREATE SEQUENCE seq_MENUID
INCREMENT BY 1 -- 每次加几个
START WITH 2053 -- 从1开始计数
NOMAXvalue -- 不设置最大值
NOCYCLE -- 一直累加，不循环
CACHE 10;
/

--主键队列
CREATE SEQUENCE seq_PostID
INCREMENT BY 1 -- 每次加几个
START WITH 10 -- 从1开始计数
NOMAXvalue -- 不设置最大值
NOCYCLE -- 一直累加，不循环
CACHE 10;
/

--主键队列
CREATE SEQUENCE seq_RoleID
INCREMENT BY 1 -- 每次加几个
START WITH 10 -- 从1开始计数
NOMAXvalue -- 不设置最大值
NOCYCLE -- 一直累加，不循环
CACHE 10;
/
